#!/usr/bin/env python3

# TODO:
# 1. Delete or finalize word if the number is entered
# 2. Rename every *set variable to something that doesn't reference built in
#    functions or types

def get_input():
    word = input("> ")
    return word.upper()


def message(text="", header="", counter="", top_sep=38, bot_sep=38):
    headerline=""

    if header and counter: headerline = f'-{header}-{counter}-'
    elif header: headerline = f'-{header}-'
    elif counter: headerline = f'-{counter}-'
    else: print('-' * top_sep)

    print(f'{headerline}' + '-' * (top_sep - len(headerline)))

    if text: print(text)
    if bot_sep: print('-' * bot_sep)


def word_check(word, userwords, pairset, uniqchars):
    # Check if the word contains the same characters as the given set
    for c in word:
        if c not in uniqchars:
            return message("[!]: В слове есть буквы, которых нет\nв наборе.")

    if word in pairset:
        message("[!]: Это слово принадлежит набору пар.")
    elif word in userwords:
        message("[!]: Такое слово уже есть.")
    else:
        # FIXME: Only check, append in main()
        userwords.append(word)


def make_charset(pairset):
    charset = []
    for pair in pairset:
        for i in range(len(pair[0])):
            if pair[0][i] != pair[1][i]:
                charset.append(pair[0][i])
                charset.append(pair[1][i])

    return charset


def get_uniq_chars(charset):
    uniqchars = []
    for i in charset:
        if i not in uniqchars:
            uniqchars.append(i)

    return uniqchars


def unnest(nested_list):
    flat_list = []
    for nest in nested_list:
        for item in nest:
            flat_list.append(item)

    return flat_list


def main():
    pairset = []
    pairlist = []
    userwords = []

    pairsets = [
        [["СЫР", "ПИР"], ["ЛАК", "ЛУК"], ["КОТ", "КИТ"], ["РАК", "РАЙ"], ["ТОК", "СОК"]],
        [["МАК", "МИР"], ["НОС", "НИЗ"], ["РАК", "РАЙ"], ["ГАД", "ГАЗ"], ["КОД", "ГОД"]],
        [["СЕРА", "МИНА"], ["ЛАК", "ЛУК"], ["КОД", "КИТ"], ["ГАД", "ГАЗ"], ["ТАКТ", "ФАКТ"]],
        [["СЫР", "ПИР"], ["ЛАК", "ЛУК"], ["КОТ", "КИТ"], ["РАК", "РАЙ"], ["ТОК", "СОК"], ["КРОТ", "ПОЛК"]]
    ]

    # Choose pairset
    message("Выберите набор пар:")

    for pairset in pairsets:
        templist = unnest(pairset)
        message(', '.join(templist), counter=pairsets.index(pairset)+1)

    pairset = pairsets[int(get_input())-1]

    # Convert nested pairset to flat list
    pairlist = unnest(pairset)

    # Create a list of unique characters
    uniqchars = get_uniq_chars(make_charset(pairset))
    uniqchars.append("Ь")

    message("Слова из набора пар нельзя использовать\nв игре.")

    while True:
        message(', '.join(uniqchars), header="Набор", counter=pairsets.index(pairset)+1)
        word_check(get_input(), userwords, pairlist, uniqchars)

        message(header="Слова", bot_sep=None, counter=len(userwords))

        for i in range(0, len(userwords)):
            # Mark long words
            if len(userwords[i]) >= 6:
                print(f'{i+1}. {userwords[i]} ({len(userwords[i])})')
            else:
                print(f'{i+1}. {userwords[i]}')


if __name__ == '__main__':
    main()
